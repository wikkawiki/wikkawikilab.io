---
layout: post
title: WikkaWiki LAB
cover: wikkawikiWizzard.png
date:   2018-08-26 12:00:00
categories: posts
---

## WikkaWiki LAB 

Building a WikkaWiki CI lab, and plugin set test.

### Boards:
- [General Organization](https://gitlab.com/groups/wikkawiki/-/boards?=)
    -  [Local documentation page](https://gitlab.com/wikkawiki/wikkawiki.gitlab.io/boards?=)
    -  [Integration Project](https://gitlab.com/wikkawiki/wikkawiki-ci/boards?=)


### Alternate links

- [Arquitectura del Software](http://arsoft12.gitlab.io/)
  - [Documentos](http://arsoft12.gitlab.io/posts/)
  - [Presentaciones](http://arsoft12.gitlab.io/slide/)
- [Arquitectura del Hardware](http://arquh11.gitlab.io/)
  - [Documentos](http://arquh11.gitlab.io/posts/)
  - [Presentaciones](http://arquh11.gitlab.io/slide/)
